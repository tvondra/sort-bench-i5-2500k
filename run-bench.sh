#!/usr/bin/env bash

PATH_OLD=$PATH
DATADIR=/mnt/raid/sort-bench

function start_server {

	data=$1
	bins=$2
	odir=$3
	conf=$4

	export PATH=$bins/bin:$PATH_OLD

	mkdir $odir

	cp $conf $data/postgresql.conf

	killall postgres > /dev/null 2>&1
	sleep 10

	pg_ctl -D $data -l $odir/pg.log -w start > $odir/start.log 2>&1

	if [ "$?" != "0" ]; then
		echo "ERROR: starting server failed"
		exit
	fi

	pg_config > $odir/config.log 2>&1

	psql postgres -c "SELECT * FROM pg_settings" > $odir/setting.log 2>&1

	ps ax > $odir/ps.log 2>&1
}

function stop_server {

	data=$1
	odir=$2

	pg_ctl -D $data -w stop > $odir/stop.log 2>&1

}

function check_stop {

        if [ -f "stop" ]; then
                exit
        fi

}

function git_push {

	gzip $1/$2/pg.log

	git add $1/$2
	git add run.log
        git commit -m "$1 $2"
        git push origin master
}


for nrows in 100000 1000000 10000000 100000000; do

	check_stop

	mkdir $nrows


	# master with default replacement_sort_tuples value

	start_server $DATADIR /home/postgres/pg-master $nrows/master-default master-default.conf

        ./sort-bench.sh test $nrows $nrows/master-default > $nrows/master-default/bench.log 2>&1

	stop_server $DATADIR $nrows/master-default

	git_push $nrows master-default

	check_stop


	# patched with entirely removed replacement sort

	start_server $DATADIR /home/postgres/pg-no-replacement-sort $nrows/no-replacement-sort no-replacement-sort.conf

       	./sort-bench.sh test $nrows $nrows/no-replacement-sort > $nrows/no-replacement-sort/bench.log 2>&1

	stop_server $DATADIR $nrows/no-replacement-sort

	git_push $nrows no-replacement-sort

	check_stop


	# master with replacement_sort_tuples increased to 1 billion

	start_server $DATADIR /home/postgres/pg-master $nrows/master-1000000000 master-1000000000.conf

        ./sort-bench.sh test $nrows $nrows/master-1000000000 > $nrows/master-1000000000/bench.log 2>&1

	stop_server $DATADIR $nrows/master-1000000000

	git_push $nrows master-1000000000

	check_stop

done
